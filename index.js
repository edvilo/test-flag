const { flags } = require('./flags.json');

const getFlag = (search) => {
  return flags.find(
    (flag) => 
      flag.flagName.toLowerCase() === search.toLowerCase() ||
      flag.label.toLowerCase() === search.toLowerCase() ||
      flag.description.toLowerCase() === search.toLowerCase()
  ) || null;
}

const getFlagId = (search) => {
  return flags.find(
    (flag) => 
      flag.flagName.toLowerCase() === search.toLowerCase() ||
      flag.label.toLowerCase() === search.toLowerCase() ||
      flag.description.toLowerCase() === search.toLowerCase()
  )?.id || null;
}

module.exports = {
  getFlag,
  getFlagId,
};
