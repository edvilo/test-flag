export declare const getFlag: (search: string) => {
    id: number;
    flagName: string;
    description: string;
    country_id: string;
    label: string;
    country: string;
} | null;
export declare const getFlagId: (search: string) => number | null;
