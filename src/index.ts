import { flags } from './flags.json';

export const getFlag = (search: string, country = 'cl') => {
  return (
    flags.find(
      (flag) =>
        (flag.flagName.toLowerCase() === search.toLowerCase() ||
          flag.label.toLowerCase() === search.toLowerCase() ||
          flag.description.toLowerCase() === search.toLowerCase())
        &&
        (flag.country.toLowerCase() === country.toLowerCase()
          || flag.country_id.toLowerCase() === country.toLowerCase())
    ) || null
  );
};

export const getFlagId = (search: string, country = 'cl') => {
  return (
    flags.find(
      (flag) =>
        (flag.flagName.toLowerCase() === search.toLowerCase() ||
          flag.label.toLowerCase() === search.toLowerCase() ||
          flag.description.toLowerCase() === search.toLowerCase())
        &&
        (flag.country.toLowerCase() === country.toLowerCase()
          || flag.country_id.toLowerCase() === country.toLowerCase())
    )?.id || null
  );
};
